const express = require('express');
const { MongoClient } = require('mongodb');
const cors = require('cors'); // Import the cors middleware
const bodyParser = require('body-parser');
const app = express();
app.use(express.json());
app.use(cors()); // Use the cors middleware to enable cross-origin requests

const uri = 'mongodb+srv://ajai:ajai@cluster0.sdm0wma.mongodb.net/?retryWrites=true&w=majority';
const dbName = 'anime';
const collectionName = 'videoapi';
const movieapi='movieapi'
const mydb='mydb'
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/', (req, res) => {
  const path = require('path');
  const indexPath = path.join(__dirname, 'public', 'index.html');
  res.sendFile(indexPath);
})
app.post('/vid', async (req, res) => {
  const tmdbid = req.body.tmdbid;
  const epid = req.body.epid;

  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

  try {
    await client.connect();
    const database = client.db(dbName);
    const collection = database.collection(collectionName);

    const document = await collection.findOne(
      { "episodes.epid": epid },
      { projection: { "collectionid": 1, "episodes.$": 1 } }
    );

    if (document) {
      const { collectionid, episodes } = document;
      const { epid: episodeEpid, episode } = episodes[0];

      console.log(`collectionid: ${collectionid}`);
      console.log(`episodeEpid: ${episodeEpid}`);
      console.log(`episode: ${episode}`);

      // Send the response back to the client
      res.json({ collectionid, episodeEpid, episode });
    } else {
      console.log("Document not found");
      res.status(404).json({ error: "Document not found" });
    }

  } finally {
    await client.close();
  }
});

app.post('/mov', async (req, res) => {
  const tmdbid = req.body.tmdbid;
  const epid = req.body.epid;

  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

  try {
    await client.connect();
    const database = client.db(dbName);
    const collection = database.collection(movieapi);

    const document = await collection.findOne(
     {tmdbid:tmdbid}
    );

    if (document) {
      const { collectionid, episode } = document;
      console.log(`collectionid: ${collectionid}`);
      console.log(`episode: ${episode}`);

      // Send the response back to the client
      res.json({collectionid, episode});
    } else {
      console.log("Document not found");
      res.status(404).json({ error: "Document not found" });
    }

  } finally {
    await client.close();
  }
});
app.post('/post', async (req, res) => {
  const tmdbid = req.body.tmdbid;
  const vidurl = req.body.vidurl;
  console.log(tmdbid, vidurl);

  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

  try {
    await client.connect();
    const database = client.db(dbName);
    const collection = database.collection(collectionName);

    // Use insertOne to insert a single document
    const result = await collection.insertOne({ tmdbid, vidurl });
    // Check the result to see if the insertion was successful
    if (result.acknowledged === true) {
      console.log('Document inserted successfully.');
     res.redirect("/");
    } else {
      console.log('Error inserting document.');
      res.status(500).send('Error inserting document.');
    }
  } finally {
    await client.close();
  }
});

app.get('/mydb', async (req, res) => {
  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  try {
    await client.connect();
    const database = client.db(dbName);
    const collection = database.collection(mydb);

    const documents = await collection.find().toArray();

    if (documents.length > 0) {
      res.json(documents);
    } else {
      console.log("Documents not found");
      res.status(404).json({ error: "Documents not found" });
    }

  } catch (error) {
    console.error("Error fetching documents:", error);
    res.status(500).json({ error: "Internal Server Error" });
  } finally {
    await client.close();
  }
});

app.get('/anime', async (req, res) => {
  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  try {
    await client.connect();
    const database = client.db(dbName);
    const collection = database.collection(mydb);

    const documents = await collection.find({"media_type":'anime'}).toArray();

    if (documents.length > 0) {
      res.json(documents);
    } else {
      console.log("Documents not found");
      res.status(404).json({ error: "Documents not found" });
    }

  } catch (error) {
    console.error("Error fetching documents:", error);
    res.status(500).json({ error: "Internal Server Error" });
  } finally {
    await client.close();
  }
});

app.get('/tv', async (req, res) => {
  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  try {
    await client.connect();
    const database = client.db(dbName);
    const collection = database.collection(mydb);

    const documents = await collection.find({media_type:'tv'}).toArray();

    if (documents.length > 0) {
      res.json(documents);
    } else {
      console.log("Documents not found");
      res.status(404).json({ error: "Documents not found" });
    }

  } catch (error) {
    console.error("Error fetching documents:", error);
    res.status(500).json({ error: "Internal Server Error" });
  } finally {
    await client.close();
  }
});
app.listen(3001, () => {
  console.log('Server is running on port 3001');
});
